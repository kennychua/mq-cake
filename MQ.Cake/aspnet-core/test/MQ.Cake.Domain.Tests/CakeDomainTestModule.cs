﻿using MQ.Cake.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace MQ.Cake
{
    [DependsOn(
        typeof(CakeEntityFrameworkCoreTestModule)
        )]
    public class CakeDomainTestModule : AbpModule
    {

    }
}