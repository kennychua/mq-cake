﻿using Volo.Abp.Modularity;

namespace MQ.Cake
{
    [DependsOn(
        typeof(CakeApplicationModule),
        typeof(CakeDomainTestModule)
        )]
    public class CakeApplicationTestModule : AbpModule
    {

    }
}