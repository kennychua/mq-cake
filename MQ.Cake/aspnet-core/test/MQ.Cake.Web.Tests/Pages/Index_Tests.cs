﻿using System.Threading.Tasks;
using Shouldly;
using Xunit;

namespace MQ.Cake.Pages
{
    public class Index_Tests : CakeWebTestBase
    {
        [Fact]
        public async Task Welcome_Page()
        {
            var response = await GetResponseAsStringAsync("/");
            response.ShouldNotBeNull();
        }
    }
}
