﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace ContentManagement
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(ContentManagementDomainSharedModule)
    )]
    public class ContentManagementDomainModule : AbpModule
    {

    }
}
