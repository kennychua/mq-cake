﻿using Volo.Abp.Settings;

namespace ContentManagement.Settings
{
    public class ContentManagementSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from ContentManagementSettings class.
             */
        }
    }
}