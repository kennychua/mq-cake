﻿namespace ContentManagement
{
    public static class ContentManagementDbProperties
    {
        public static string DbTablePrefix { get; set; } = "ContentManagement";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "ContentManagement";
    }
}
