﻿using Volo.Abp.Localization;

namespace ContentManagement.Localization
{
    [LocalizationResourceName("ContentManagement")]
    public class ContentManagementResource
    {
        
    }
}
