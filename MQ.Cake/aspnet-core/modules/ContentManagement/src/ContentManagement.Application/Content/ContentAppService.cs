﻿using System.Threading.Tasks;
using ContentManagement.Samples;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using System.Collections.Generic;
using System;
using Newtonsoft.Json;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;

namespace ContentManagement.Content
{
    public class ContentAppService : ContentManagementAppService, IContentAppService
    {
        private const string BaseUrl = "https://cloud.squidex.io/api/content/ilearn/content";
        private const string bearerToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjlTZWo3eXJYdGo1TzNDVV9JeG5EU2ciLCJ0eXAiOiJhdCtqd3QifQ.eyJuYmYiOjE2MTE1Mzk3NDksImV4cCI6MTYxNDEzMTc0OSwiaXNzIjoiaHR0cHM6Ly9jbG91ZC5zcXVpZGV4LmlvL2lkZW50aXR5LXNlcnZlciIsImNsaWVudF9pZCI6ImlsZWFybjpkZWZhdWx0IiwianRpIjoiOTI3MkI4REEwODkyQzBDRTAxNjJEMzk4OEEwQjQ0NDQiLCJpYXQiOjE2MTE1Mzk3NDksInNjb3BlIjpbInNxdWlkZXgtYXBpIl19.AACuCuRvAhycvLOfypczwtSsFTIku3eMrft0kEbPi36OhPmDu4eznUSuf3tNNBxVRzL2k8iq2Mcx1KbMtvsTa0BqE4_BoXiLU-joxb75k_XwAbRoT6qzaGvnvK2RaRP1xbsu2tDqdDluFMa7tYh8SCNeXrudWdGhgniVtR-DfHYAgcE6DClEq_s_C6R_5w2Qw0BGaTIYk1z3oB_wK4XPb70A-uaSGBIyTpo-7EK0fCSh3CzsCOZH1o5xFyBq9fG2pQArLfhOI8VkgkylmwzDDScmev3lYo2k7SVBlMCVtlA6t-NDJZG9y7KRmstFylZzX1gMRMYSDjy_VOiKlPSIvQ";

        public ContentAppService()
        {
           
        }

        public async Task<List<Items>> GetAsync()
        {

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization
                = new AuthenticationHeaderValue("Bearer", bearerToken);

                var httpResponse = await client.GetAsync(BaseUrl);

                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception("Cannot retrieve tasks");
                }

                var content = await httpResponse.Content.ReadAsStringAsync();
                var tasks = JsonConvert.DeserializeObject<Response>(content);

                List<Items> allItems = tasks.items;


                return allItems;
            }
        }

        public async Task<Items> GetAsyncById(string guid)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization
                = new AuthenticationHeaderValue("Bearer", bearerToken);

                var url = BaseUrl + "/" + guid;

                var httpResponse = await client.GetAsync(url);

                if (!httpResponse.IsSuccessStatusCode)
                {
                    throw new Exception("Cannot retrieve tasks");
                }

                var content = await httpResponse.Content.ReadAsStringAsync();
                var item = JsonConvert.DeserializeObject<Items>(content);

                return item;
            }
        }

        public async Task<PostResponse> CreateAsync(ContentDto contentDto)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization
                = new AuthenticationHeaderValue("Bearer", bearerToken);

                var contentString = JsonConvert.SerializeObject(contentDto);

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri(BaseUrl + "?publish=true"),
                    Content = new StringContent(contentString)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue("application/json")
                        }
                    }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(body);

                    var returnedJsonObj = JsonConvert.DeserializeObject<PostResponse>(body);

                    return returnedJsonObj;
                }
            }
        }

        public async Task<PostResponse> UpdateAsync(string guid)
        {

            var itemToBeModified = await this.GetAsyncById(guid);

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization
                = new AuthenticationHeaderValue("Bearer", bearerToken);

                var url = BaseUrl + "/" + guid;

                itemToBeModified.data.contentName.en = "ModifiedName";
                var contentString = JsonConvert.SerializeObject(itemToBeModified.data);

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = HttpMethod.Put,
                    RequestUri = new Uri(url),
                    Content = new StringContent(contentString)
                    {
                        Headers =
                        {
                            ContentType = new MediaTypeHeaderValue("application/json")
                        }
                    }
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(body);

                    return JsonConvert.DeserializeObject<PostResponse>(body);
                }
            }
        }

        public async Task<DeleteResponse> DeleteAsync(string guid)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Authorization
                = new AuthenticationHeaderValue("Bearer", bearerToken);

                var url = BaseUrl + "/" + guid;

                HttpRequestMessage request = new HttpRequestMessage
                {
                    Method = HttpMethod.Delete,
                    RequestUri = new Uri(url),
                };

                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();

                    DeleteResponse returnResponse = new DeleteResponse();
                    returnResponse.message = "Delete Successful!";
                    return returnResponse;

                }
            }
        }
    }
}
