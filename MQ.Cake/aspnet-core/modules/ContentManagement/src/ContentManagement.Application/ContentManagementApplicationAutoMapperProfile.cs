﻿using AutoMapper;

namespace ContentManagement
{
    public class ContentManagementApplicationAutoMapperProfile : Profile
    {
        public ContentManagementApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}