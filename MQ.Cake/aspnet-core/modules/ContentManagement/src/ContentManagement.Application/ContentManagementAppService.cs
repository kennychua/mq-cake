﻿using ContentManagement.Localization;
using Volo.Abp.Application.Services;

namespace ContentManagement
{
    public abstract class ContentManagementAppService : ApplicationService
    {
        protected ContentManagementAppService()
        {
            LocalizationResource = typeof(ContentManagementResource);
            ObjectMapperContext = typeof(ContentManagementApplicationModule);
        }
    }
}
