﻿using Volo.Abp.Reflection;

namespace ContentManagement.Permissions
{
    public class ContentManagementPermissions
    {
        public const string GroupName = "ContentManagement";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(ContentManagementPermissions));
        }
    }
}