﻿using System.Collections.Generic;

namespace ContentManagement.Content
{
    public class ContentDto
    {
        /**
         * Constructor 
         */
        public ContentDto(DescriptionClass contentDescription, NameClass contentName, TypeClass contentType, DurationClass contentDuration, AssetClass contentAsset)
        {
            this.contentDescription = contentDescription;
            this.contentName = contentName;
            this.contentType = contentType;
            this.contentDuration = contentDuration;
            this.contentAsset = contentAsset;
        }

        public NameClass contentName { get; set; }
        public DescriptionClass contentDescription { get; set; }
        public TypeClass contentType { get; set; }
        public DurationClass contentDuration { get; set; }
        public AssetClass contentAsset { get; set; }

        public class NameClass
        {
            public string en { get; set; }
            public NameClass(string en)
            {
                this.en = en;
            }
        }
        public class DescriptionClass
        {
            public string en { get; set; }
            public DescriptionClass(string en)
            {
                this.en = en;
            }
        }
        public class TypeClass
        {
            public string en { get; set; }
            public TypeClass(string en)
            {
                this.en = en;
            }
        }

        public class DurationClass
        {
            public int iv { get; set; }
            public DurationClass(int iv)
            {
                this.iv = iv;
            }
        }
        /**
         * For handling the content asset class
         * 
         */
        public class AssetClass
        {
            public List<string> iv { get; set; }
            public AssetClass(List<string> iv)
            {
                this.iv = iv;
            }

            public string getFullUri(string contentAsset, string baseUrl)
            {
                return (baseUrl + contentAsset);
            }
        }

    }
}