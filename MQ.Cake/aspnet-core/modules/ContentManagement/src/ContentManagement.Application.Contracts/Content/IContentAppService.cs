﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Volo.Abp.Application.Services;

namespace ContentManagement.Content
{
    public interface IContentAppService : IApplicationService
    {
        Task<List<Items>> GetAsync();

        Task<Items> GetAsyncById(string guid);

        Task<PostResponse> CreateAsync(ContentDto contentDto);

        Task<PostResponse> UpdateAsync(string guid);

        Task<DeleteResponse> DeleteAsync(string guid);
    }
}
