﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace ContentManagement
{
    [DependsOn(
        typeof(ContentManagementDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class ContentManagementApplicationContractsModule : AbpModule
    {

    }
}
