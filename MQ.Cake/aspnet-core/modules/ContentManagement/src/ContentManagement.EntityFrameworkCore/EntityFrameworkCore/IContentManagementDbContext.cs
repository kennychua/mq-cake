﻿using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace ContentManagement.EntityFrameworkCore
{
    [ConnectionStringName(ContentManagementDbProperties.ConnectionStringName)]
    public interface IContentManagementDbContext : IEfCoreDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * DbSet<Question> Questions { get; }
         */
    }
}