﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace ContentManagement.EntityFrameworkCore
{
    public class ContentManagementModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {
        public ContentManagementModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {

        }
    }
}