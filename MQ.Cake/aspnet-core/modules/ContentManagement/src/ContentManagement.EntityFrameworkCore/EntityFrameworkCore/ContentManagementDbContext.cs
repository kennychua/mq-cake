﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace ContentManagement.EntityFrameworkCore
{
    [ConnectionStringName(ContentManagementDbProperties.ConnectionStringName)]
    public class ContentManagementDbContext : AbpDbContext<ContentManagementDbContext>, IContentManagementDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * public DbSet<Question> Questions { get; set; }
         */

        public ContentManagementDbContext(DbContextOptions<ContentManagementDbContext> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ConfigureContentManagement();
        }
    }
}