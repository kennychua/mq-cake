﻿using ContentManagement.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace ContentManagement.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class ContentManagementPageModel : AbpPageModel
    {
        protected ContentManagementPageModel()
        {
            LocalizationResourceType = typeof(ContentManagementResource);
            ObjectMapperContext = typeof(ContentManagementWebModule);
        }
    }
}