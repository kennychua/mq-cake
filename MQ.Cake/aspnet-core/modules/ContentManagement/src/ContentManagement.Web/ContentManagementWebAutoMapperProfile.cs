﻿using AutoMapper;

namespace ContentManagement.Web
{
    public class ContentManagementWebAutoMapperProfile : Profile
    {
        public ContentManagementWebAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}