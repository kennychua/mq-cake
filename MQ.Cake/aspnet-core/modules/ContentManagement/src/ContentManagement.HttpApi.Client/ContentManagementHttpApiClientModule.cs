﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace ContentManagement
{
    [DependsOn(
        typeof(ContentManagementApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class ContentManagementHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "ContentManagement";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(ContentManagementApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
