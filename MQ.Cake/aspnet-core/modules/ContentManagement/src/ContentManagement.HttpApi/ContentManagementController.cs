﻿using ContentManagement.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace ContentManagement
{
    public abstract class ContentManagementController : AbpController
    {
        protected ContentManagementController()
        {
            LocalizationResource = typeof(ContentManagementResource);
        }
    }
}
