﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Volo.Abp;

namespace ContentManagement.Content
{
    [RemoteService]
    [Route("api/ContentManagement/content")]
    public class ContentController : ContentManagementController, IContentAppService
    {
        private readonly IContentAppService _contentAppService;

        public ContentController(IContentAppService contentAppService)
        {
            _contentAppService = contentAppService;
        }

        [HttpGet]
        public async Task<List<Items>> GetAsync()
        {

            return await _contentAppService.GetAsync();
        }

        [HttpGet("{guid}")]
        public async Task<Items> GetAsyncById(string guid)
        {
            return await _contentAppService.GetAsyncById(guid);
        }

        [HttpPost]
        public async Task<PostResponse> CreateAsync(ContentDto contentDto)
        {
            return await _contentAppService.CreateAsync(contentDto);
        }

        [HttpPut("{guid}")]
        public async Task<PostResponse> UpdateAsync(string guid)
        {
            return await _contentAppService.UpdateAsync(guid);
        }

        [HttpDelete("{guid}")]
        public async Task<DeleteResponse> DeleteAsync(string guid)
        {
            return await _contentAppService.DeleteAsync(guid);
        }
    }
}
