﻿using Localization.Resources.AbpUi;
using ContentManagement.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace ContentManagement
{
    [DependsOn(
        typeof(ContentManagementApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class ContentManagementHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(ContentManagementHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<ContentManagementResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
