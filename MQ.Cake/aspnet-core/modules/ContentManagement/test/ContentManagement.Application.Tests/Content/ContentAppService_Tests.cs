﻿using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ContentManagement.Content
{
    public class ContentAppService_Tests : ContentManagementApplicationTestBase
    {
        private readonly IContentAppService _contentAppService;

        public ContentAppService_Tests()
        {
            _contentAppService = GetRequiredService<IContentAppService>();
        }

        [Fact]
        public async Task GetAsync()
        {

            var result = await _contentAppService.GetAsync();

            //Assert
            result.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task GetAsyncById()
        {
            var alldata = await _contentAppService.GetAsync();

            var id = alldata.FirstOrDefault().id;

            var result = await _contentAppService.GetAsyncById(id);

            //Assert
            result.id.ShouldNotBeNull();
        }

        [Fact]
        public async Task CreateAsync()
        {
            ContentDto contentDto = new ContentDto(
                                    new ContentDto.DescriptionClass("TestDesc"),
                                    new ContentDto.NameClass("TestName"), 
                                    new ContentDto.TypeClass("TestType"),
                                    new ContentDto.DurationClass(10),
                                    new ContentDto.AssetClass(new List<string>() { "TestUri" }));

            var result = await _contentAppService.CreateAsync(contentDto);

            //Assert
            result.id.ShouldNotBeNull();
            result.data.contentName.en.ShouldBe("TestName");
        }

        [Fact]
        public async Task UpdateAsync()
        {
            var alldata = await _contentAppService.GetAsync();

            var id = alldata.FirstOrDefault().id;

            var result = await _contentAppService.UpdateAsync(id);

            //Assert
            result.data.contentName.en.ShouldBe("ModifiedName");
        }

        [Fact]
        public async Task DeleteAsync()
        {
            var alldata = await _contentAppService.GetAsync();

            //var id = alldata.Where(x => x.data.contentName.Equals("TestName")).FirstOrDefault().id;
            var id = alldata.FirstOrDefault().id;

            var result = await _contentAppService.DeleteAsync(id);

            //Assert
            result.message.ShouldBe("Delete Successful!");
        }
    }
}
