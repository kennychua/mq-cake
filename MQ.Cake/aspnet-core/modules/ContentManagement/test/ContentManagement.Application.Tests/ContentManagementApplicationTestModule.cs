﻿using Volo.Abp.Modularity;

namespace ContentManagement
{
    [DependsOn(
        typeof(ContentManagementApplicationModule),
        typeof(ContentManagementDomainTestModule)
        )]
    public class ContentManagementApplicationTestModule : AbpModule
    {

    }
}
