﻿using ContentManagement.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace ContentManagement
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(ContentManagementEntityFrameworkCoreTestModule)
        )]
    public class ContentManagementDomainTestModule : AbpModule
    {
        
    }
}
