﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace ContentManagement
{
    [DependsOn(
        typeof(ContentManagementHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class ContentManagementConsoleApiClientModule : AbpModule
    {
        
    }
}
