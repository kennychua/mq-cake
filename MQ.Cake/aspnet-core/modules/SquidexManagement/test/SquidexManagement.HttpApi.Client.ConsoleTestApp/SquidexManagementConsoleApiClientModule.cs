﻿using Volo.Abp.Http.Client.IdentityModel;
using Volo.Abp.Modularity;

namespace SquidexManagement
{
    [DependsOn(
        typeof(SquidexManagementHttpApiClientModule),
        typeof(AbpHttpClientIdentityModelModule)
        )]
    public class SquidexManagementConsoleApiClientModule : AbpModule
    {
        
    }
}
