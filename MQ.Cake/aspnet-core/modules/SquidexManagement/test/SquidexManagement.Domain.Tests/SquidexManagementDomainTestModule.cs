﻿using SquidexManagement.EntityFrameworkCore;
using Volo.Abp.Modularity;

namespace SquidexManagement
{
    /* Domain tests are configured to use the EF Core provider.
     * You can switch to MongoDB, however your domain tests should be
     * database independent anyway.
     */
    [DependsOn(
        typeof(SquidexManagementEntityFrameworkCoreTestModule)
        )]
    public class SquidexManagementDomainTestModule : AbpModule
    {
        
    }
}
