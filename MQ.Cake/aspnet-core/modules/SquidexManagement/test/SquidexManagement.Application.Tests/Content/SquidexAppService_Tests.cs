﻿using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace SquidexManagement.Content
{
    public class SquidexAppService_Tests : SquidexManagementApplicationTestBase
    {
       // private readonly ISquidexAppService _squidexAppService;

        public SquidexAppService_Tests()
        {
           // _squidexAppService = GetRequiredService<ISquidexAppService>();
        }

        private ContentContentDto generateTestContentContentDto()
        {
            ContentContentDto testContentContent = new ContentContentDto();
            testContentContent.Id = "1";

            ContentDto testContent = new ContentDto();
            testContent.ContentName = new ContentContentNameProperty();
            testContent.ContentName.En = "Ilearn Pilot Charter modified from abp.io";

            testContent.ContentDescription = new ContentContentDescriptionProperty();
            testContent.ContentDescription.En = "Ilearn Pilot Charter from postman";

            testContent.ContentType = new ContentContentTypeProperty();
            testContent.ContentType.En = "pdf";

            testContent.ContentDuration = new ContentContentDurationProperty();
            testContent.ContentDuration.Iv = 56;

            testContentContent.Data = testContent;

            testContentContent.Created = new DateTimeOffset();
            testContentContent.CreatedBy = "Test";
            testContentContent.LastModified = new DateTimeOffset();
            testContentContent.LastModifiedBy = "Test";
            testContentContent.NewStatus = null;
            testContentContent.Status = "Published";

            return testContentContent;
        }

        [Fact]
        public async Task GetAsync()
        {
            Response testResponse = new Response();
            testResponse.Total = 1;

            testResponse.Items.Add(generateTestContentContentDto());

            var contentString = JsonConvert.SerializeObject(testResponse);

            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(contentString),
            };

            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>())
               .ReturnsAsync(response);
            var httpClient = new HttpClient(handlerMock.Object);

            var squidexAppService = new SquidexAppService(httpClient);

            var result = await squidexAppService.QueryContentContentsAsync("", null, null, "", "");
            result.Items.Count.ShouldBeGreaterThan(0);

            //int i = 0;
            //i.ShouldBe(0);
        }

        [Fact]
        public async Task CreateContentContentAsync()
        {

            ContentContentDto testContentContentDto = new ContentContentDto();
            testContentContentDto = generateTestContentContentDto();

            var contentString = JsonConvert.SerializeObject(testContentContentDto);

            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.Created,
                Content = new StringContent(contentString),
            };

            handlerMock
              .Protected()
              .Setup<Task<HttpResponseMessage>>(
                 "SendAsync",
                 ItExpr.IsAny<HttpRequestMessage>(),
                 ItExpr.IsAny<CancellationToken>())
              .ReturnsAsync(response);
            var httpClient = new HttpClient(handlerMock.Object);

            var squidexAppService = new SquidexAppService(httpClient);

            var result = await squidexAppService.CreateContentContentAsync(true, "TestId", testContentContentDto.Data);

            result.Id.ShouldNotBeNull();
            result.Data.ContentName.En.ShouldBe("Ilearn Pilot Charter modified from abp.io");
        }

        [Fact]
        public async Task GetContentContentAsync()
        {
            ContentContentDto testContentContentDto = new ContentContentDto();
            testContentContentDto = generateTestContentContentDto();

            var contentString = JsonConvert.SerializeObject(testContentContentDto);

            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(contentString),
            };

            handlerMock
              .Protected()
              .Setup<Task<HttpResponseMessage>>(
                 "SendAsync",
                 ItExpr.IsAny<HttpRequestMessage>(),
                 ItExpr.IsAny<CancellationToken>())
              .ReturnsAsync(response);
            var httpClient = new HttpClient(handlerMock.Object);

            var squidexAppService = new SquidexAppService(httpClient);

            var result = await squidexAppService.GetContentContentAsync("1");

            //Assert
            result.Id.ShouldNotBeNull();
        }

        [Fact]
        public async Task UpdateContentContentAsync()
        {
            ContentContentDto testContentContentDto = new ContentContentDto();
            testContentContentDto = generateTestContentContentDto();

            var contentString = JsonConvert.SerializeObject(testContentContentDto);

            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(contentString),
            };

            handlerMock
              .Protected()
              .Setup<Task<HttpResponseMessage>>(
                 "SendAsync",
                 ItExpr.IsAny<HttpRequestMessage>(),
                 ItExpr.IsAny<CancellationToken>())
              .ReturnsAsync(response);
            var httpClient = new HttpClient(handlerMock.Object);

            var squidexAppService = new SquidexAppService(httpClient);

            var result = await squidexAppService.UpdateContentContentAsync("1", testContentContentDto.Data);

            result.Id.ShouldNotBeNull();
        }

        [Fact]
        public async Task PathContentContentAsync()
        {
            ContentContentDto testContentContentDto = new ContentContentDto();
            testContentContentDto = generateTestContentContentDto();

            var contentString = JsonConvert.SerializeObject(testContentContentDto);

            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent(contentString),
            };

            handlerMock
              .Protected()
              .Setup<Task<HttpResponseMessage>>(
                 "SendAsync",
                 ItExpr.IsAny<HttpRequestMessage>(),
                 ItExpr.IsAny<CancellationToken>())
              .ReturnsAsync(response);
            var httpClient = new HttpClient(handlerMock.Object);

            var squidexAppService = new SquidexAppService(httpClient);

            var result = await squidexAppService.PathContentContentAsync("1", testContentContentDto.Data);

            result.Id.ShouldNotBeNull();
        }

        [Fact]
        public async Task DeleteContentContentAsync()
        {

            var handlerMock = new Mock<HttpMessageHandler>();
            var response = new HttpResponseMessage
            {
                StatusCode = HttpStatusCode.NoContent
            };

            handlerMock
              .Protected()
              .Setup<Task<HttpResponseMessage>>(
                 "SendAsync",
                 ItExpr.IsAny<HttpRequestMessage>(),
                 ItExpr.IsAny<CancellationToken>())
              .ReturnsAsync(response);
            var httpClient = new HttpClient(handlerMock.Object);

            var squidexAppService = new SquidexAppService(httpClient);
            await squidexAppService.DeleteContentContentAsync("1");

            response.StatusCode.ShouldBe(HttpStatusCode.NoContent);

        }
    }
}
