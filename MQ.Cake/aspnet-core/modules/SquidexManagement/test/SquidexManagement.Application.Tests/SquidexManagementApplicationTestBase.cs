﻿namespace SquidexManagement
{
    /* Inherit from this class for your application layer tests.
     * See SampleAppService_Tests for example.
     */
    public abstract class SquidexManagementApplicationTestBase : SquidexManagementTestBase<SquidexManagementApplicationTestModule>
    {

    }
}