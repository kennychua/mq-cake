﻿using Volo.Abp.Modularity;

namespace SquidexManagement
{
    [DependsOn(
        typeof(SquidexManagementApplicationModule),
        typeof(SquidexManagementDomainTestModule)
        )]
    public class SquidexManagementApplicationTestModule : AbpModule
    {

    }
}
