﻿using SquidexManagement.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace SquidexManagement.Permissions
{
    public class SquidexManagementPermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(SquidexManagementPermissions.GroupName, L("Permission:SquidexManagement"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<SquidexManagementResource>(name);
        }
    }
}