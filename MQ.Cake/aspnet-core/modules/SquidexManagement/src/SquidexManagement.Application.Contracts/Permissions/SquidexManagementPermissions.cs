﻿using Volo.Abp.Reflection;

namespace SquidexManagement.Permissions
{
    public class SquidexManagementPermissions
    {
        public const string GroupName = "SquidexManagement";

        public static string[] GetAll()
        {
            return ReflectionHelper.GetPublicConstantsRecursively(typeof(SquidexManagementPermissions));
        }
    }
}