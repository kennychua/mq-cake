﻿using Volo.Abp.Application;
using Volo.Abp.Modularity;
using Volo.Abp.Authorization;

namespace SquidexManagement
{
    [DependsOn(
        typeof(SquidexManagementDomainSharedModule),
        typeof(AbpDddApplicationContractsModule),
        typeof(AbpAuthorizationModule)
        )]
    public class SquidexManagementApplicationContractsModule : AbpModule
    {

    }
}
