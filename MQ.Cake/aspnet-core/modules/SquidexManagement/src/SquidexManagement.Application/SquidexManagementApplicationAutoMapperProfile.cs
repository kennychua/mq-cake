﻿using AutoMapper;

namespace SquidexManagement
{
    public class SquidexManagementApplicationAutoMapperProfile : Profile
    {
        public SquidexManagementApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}