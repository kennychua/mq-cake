﻿using SquidexManagement.Localization;
using Volo.Abp.Application.Services;

namespace SquidexManagement
{
    public abstract class SquidexManagementAppService : ApplicationService
    {
        protected SquidexManagementAppService()
        {
            LocalizationResource = typeof(SquidexManagementResource);
            ObjectMapperContext = typeof(SquidexManagementApplicationModule);
        }
    }
}
