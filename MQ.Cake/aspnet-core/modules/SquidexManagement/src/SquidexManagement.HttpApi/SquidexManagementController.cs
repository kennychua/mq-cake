﻿using SquidexManagement.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace SquidexManagement
{
    public abstract class SquidexManagementController : AbpController
    {
        protected SquidexManagementController()
        {
            LocalizationResource = typeof(SquidexManagementResource);
        }
    }
}
