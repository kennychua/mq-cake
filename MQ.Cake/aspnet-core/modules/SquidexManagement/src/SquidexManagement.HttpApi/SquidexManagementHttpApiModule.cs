﻿using Localization.Resources.AbpUi;
using SquidexManagement.Localization;
using Volo.Abp.AspNetCore.Mvc;
using Volo.Abp.Localization;
using Volo.Abp.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace SquidexManagement
{
    [DependsOn(
        typeof(SquidexManagementApplicationContractsModule),
        typeof(AbpAspNetCoreMvcModule))]
    public class SquidexManagementHttpApiModule : AbpModule
    {
        public override void PreConfigureServices(ServiceConfigurationContext context)
        {
            PreConfigure<IMvcBuilder>(mvcBuilder =>
            {
                mvcBuilder.AddApplicationPartIfNotExists(typeof(SquidexManagementHttpApiModule).Assembly);
            });
        }

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpLocalizationOptions>(options =>
            {
                options.Resources
                    .Get<SquidexManagementResource>()
                    .AddBaseTypes(typeof(AbpUiResource));
            });
        }
    }
}
