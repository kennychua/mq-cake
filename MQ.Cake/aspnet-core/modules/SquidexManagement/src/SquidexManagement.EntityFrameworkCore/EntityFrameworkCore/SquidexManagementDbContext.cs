﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace SquidexManagement.EntityFrameworkCore
{
    [ConnectionStringName(SquidexManagementDbProperties.ConnectionStringName)]
    public class SquidexManagementDbContext : AbpDbContext<SquidexManagementDbContext>, ISquidexManagementDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * public DbSet<Question> Questions { get; set; }
         */

        public SquidexManagementDbContext(DbContextOptions<SquidexManagementDbContext> options) 
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.ConfigureSquidexManagement();
        }
    }
}