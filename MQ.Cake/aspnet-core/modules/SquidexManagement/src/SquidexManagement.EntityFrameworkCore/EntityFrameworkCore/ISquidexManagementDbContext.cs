﻿using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace SquidexManagement.EntityFrameworkCore
{
    [ConnectionStringName(SquidexManagementDbProperties.ConnectionStringName)]
    public interface ISquidexManagementDbContext : IEfCoreDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * DbSet<Question> Questions { get; }
         */
    }
}