﻿using JetBrains.Annotations;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace SquidexManagement.EntityFrameworkCore
{
    public class SquidexManagementModelBuilderConfigurationOptions : AbpModelBuilderConfigurationOptions
    {
        public SquidexManagementModelBuilderConfigurationOptions(
            [NotNull] string tablePrefix = "",
            [CanBeNull] string schema = null)
            : base(
                tablePrefix,
                schema)
        {

        }
    }
}