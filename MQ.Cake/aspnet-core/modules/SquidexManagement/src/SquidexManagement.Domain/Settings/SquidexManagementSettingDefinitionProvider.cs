﻿using Volo.Abp.Settings;

namespace SquidexManagement.Settings
{
    public class SquidexManagementSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            /* Define module settings here.
             * Use names from SquidexManagementSettings class.
             */
        }
    }
}