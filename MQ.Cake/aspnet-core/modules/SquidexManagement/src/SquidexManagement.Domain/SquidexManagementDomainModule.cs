﻿using Volo.Abp.Domain;
using Volo.Abp.Modularity;

namespace SquidexManagement
{
    [DependsOn(
        typeof(AbpDddDomainModule),
        typeof(SquidexManagementDomainSharedModule)
    )]
    public class SquidexManagementDomainModule : AbpModule
    {

    }
}
