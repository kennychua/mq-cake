﻿namespace SquidexManagement
{
    public static class SquidexManagementDbProperties
    {
        public static string DbTablePrefix { get; set; } = "SquidexManagement";

        public static string DbSchema { get; set; } = null;

        public const string ConnectionStringName = "SquidexManagement";
    }
}
