﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace SquidexManagement
{
    [DependsOn(
        typeof(SquidexManagementApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class SquidexManagementHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "SquidexManagement";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(SquidexManagementApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
