﻿using Volo.Abp.Localization;

namespace SquidexManagement.Localization
{
    [LocalizationResourceName("SquidexManagement")]
    public class SquidexManagementResource
    {
        
    }
}
