﻿using AutoMapper;

namespace SquidexManagement.Web
{
    public class SquidexManagementWebAutoMapperProfile : Profile
    {
        public SquidexManagementWebAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}