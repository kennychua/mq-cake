﻿using SquidexManagement.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace SquidexManagement.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class SquidexManagementPageModel : AbpPageModel
    {
        protected SquidexManagementPageModel()
        {
            LocalizationResourceType = typeof(SquidexManagementResource);
            ObjectMapperContext = typeof(SquidexManagementWebModule);
        }
    }
}