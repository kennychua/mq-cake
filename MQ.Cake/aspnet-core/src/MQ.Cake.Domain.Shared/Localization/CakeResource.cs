﻿using Volo.Abp.Localization;

namespace MQ.Cake.Localization
{
    [LocalizationResourceName("Cake")]
    public class CakeResource
    {

    }
}