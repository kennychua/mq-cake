﻿using Volo.Abp.Ui.Branding;
using Volo.Abp.DependencyInjection;

namespace MQ.Cake.Web
{
    [Dependency(ReplaceServices = true)]
    public class CakeBrandingProvider : DefaultBrandingProvider
    {
        public override string AppName => "Cake";
    }
}
