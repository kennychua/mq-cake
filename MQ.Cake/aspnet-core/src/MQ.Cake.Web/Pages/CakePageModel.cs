﻿using MQ.Cake.Localization;
using Volo.Abp.AspNetCore.Mvc.UI.RazorPages;

namespace MQ.Cake.Web.Pages
{
    /* Inherit your PageModel classes from this class.
     */
    public abstract class CakePageModel : AbpPageModel
    {
        protected CakePageModel()
        {
            LocalizationResourceType = typeof(CakeResource);
        }
    }
}