﻿using MQ.Cake.EntityFrameworkCore;
using Volo.Abp.Autofac;
using Volo.Abp.BackgroundJobs;
using Volo.Abp.Modularity;

namespace MQ.Cake.DbMigrator
{
    [DependsOn(
        typeof(AbpAutofacModule),
        typeof(CakeEntityFrameworkCoreDbMigrationsModule),
        typeof(CakeApplicationContractsModule)
        )]
    public class CakeDbMigratorModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpBackgroundJobOptions>(options => options.IsJobExecutionEnabled = false);
        }
    }
}
