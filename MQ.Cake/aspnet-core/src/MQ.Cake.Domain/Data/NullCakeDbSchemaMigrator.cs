﻿using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;

namespace MQ.Cake.Data
{
    /* This is used if database provider does't define
     * ICakeDbSchemaMigrator implementation.
     */
    public class NullCakeDbSchemaMigrator : ICakeDbSchemaMigrator, ITransientDependency
    {
        public Task MigrateAsync()
        {
            return Task.CompletedTask;
        }
    }
}