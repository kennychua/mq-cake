﻿using System.Threading.Tasks;

namespace MQ.Cake.Data
{
    public interface ICakeDbSchemaMigrator
    {
        Task MigrateAsync();
    }
}
