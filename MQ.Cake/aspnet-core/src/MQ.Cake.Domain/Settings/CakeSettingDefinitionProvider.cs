﻿using Volo.Abp.Settings;

namespace MQ.Cake.Settings
{
    public class CakeSettingDefinitionProvider : SettingDefinitionProvider
    {
        public override void Define(ISettingDefinitionContext context)
        {
            //Define your own settings here. Example:
            //context.Add(new SettingDefinition(CakeSettings.MySetting1));
        }
    }
}
