﻿using Microsoft.EntityFrameworkCore;
using Volo.Abp;

namespace MQ.Cake.EntityFrameworkCore
{
    public static class CakeDbContextModelCreatingExtensions
    {
        public static void ConfigureCake(this ModelBuilder builder)
        {
            Check.NotNull(builder, nameof(builder));

            /* Configure your own tables/entities inside here */

            //builder.Entity<YourEntity>(b =>
            //{
            //    b.ToTable(CakeConsts.DbTablePrefix + "YourEntities", CakeConsts.DbSchema);
            //    b.ConfigureByConvention(); //auto configure for the base class props
            //    //...
            //});
        }
    }
}