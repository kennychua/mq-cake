﻿using System;
using System.Collections.Generic;
using System.Text;
using MQ.Cake.Localization;
using Volo.Abp.Application.Services;

namespace MQ.Cake
{
    /* Inherit your application services from this class.
     */
    public abstract class CakeAppService : ApplicationService
    {
        protected CakeAppService()
        {
            LocalizationResource = typeof(CakeResource);
        }
    }
}
