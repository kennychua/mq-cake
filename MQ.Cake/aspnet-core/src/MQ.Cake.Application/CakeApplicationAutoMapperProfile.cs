﻿using AutoMapper;

namespace MQ.Cake
{
    public class CakeApplicationAutoMapperProfile : Profile
    {
        public CakeApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}
