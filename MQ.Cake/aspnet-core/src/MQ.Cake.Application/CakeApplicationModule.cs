using Volo.Abp.Account;
using Volo.Abp.AutoMapper;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TenantManagement;
using ContentManagement;
using SquidexManagement;

namespace MQ.Cake
{
    [DependsOn(
        typeof(CakeDomainModule),
        typeof(AbpAccountApplicationModule),
        typeof(CakeApplicationContractsModule),
        typeof(AbpIdentityApplicationModule),
        typeof(AbpPermissionManagementApplicationModule),
        typeof(AbpTenantManagementApplicationModule),
        typeof(AbpFeatureManagementApplicationModule)
        )]
    [DependsOn(typeof(ContentManagementApplicationModule))]
    [DependsOn(typeof(SquidexManagementApplicationModule))]
    public class CakeApplicationModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            Configure<AbpAutoMapperOptions>(options =>
            {
                options.AddMaps<CakeApplicationModule>();
            });
        }
    }
}
