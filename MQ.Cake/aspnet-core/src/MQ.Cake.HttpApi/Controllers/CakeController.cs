﻿using MQ.Cake.Localization;
using Volo.Abp.AspNetCore.Mvc;

namespace MQ.Cake.Controllers
{
    /* Inherit your controllers from this class.
     */
    public abstract class CakeController : AbpController
    {
        protected CakeController()
        {
            LocalizationResource = typeof(CakeResource);
        }
    }
}