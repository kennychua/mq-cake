﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Modularity;

namespace MQ.Cake.EntityFrameworkCore
{
    [DependsOn(
        typeof(CakeEntityFrameworkCoreModule)
        )]
    public class CakeEntityFrameworkCoreDbMigrationsModule : AbpModule
    {
        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddAbpDbContext<CakeMigrationsDbContext>();
        }
    }
}
