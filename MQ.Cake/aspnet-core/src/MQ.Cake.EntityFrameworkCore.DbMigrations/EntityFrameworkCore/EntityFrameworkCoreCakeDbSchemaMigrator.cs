﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MQ.Cake.Data;
using Volo.Abp.DependencyInjection;

namespace MQ.Cake.EntityFrameworkCore
{
    public class EntityFrameworkCoreCakeDbSchemaMigrator
        : ICakeDbSchemaMigrator, ITransientDependency
    {
        private readonly IServiceProvider _serviceProvider;

        public EntityFrameworkCoreCakeDbSchemaMigrator(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task MigrateAsync()
        {
            /* We intentionally resolving the CakeMigrationsDbContext
             * from IServiceProvider (instead of directly injecting it)
             * to properly get the connection string of the current tenant in the
             * current scope.
             */

            await _serviceProvider
                .GetRequiredService<CakeMigrationsDbContext>()
                .Database
                .MigrateAsync();
        }
    }
}