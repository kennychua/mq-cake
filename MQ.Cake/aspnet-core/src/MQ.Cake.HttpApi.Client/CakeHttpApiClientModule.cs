using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Account;
using Volo.Abp.FeatureManagement;
using Volo.Abp.Identity;
using Volo.Abp.Modularity;
using Volo.Abp.PermissionManagement;
using Volo.Abp.TenantManagement;
using ContentManagement;
using SquidexManagement;
using Volo.Abp.Http.Client;

namespace MQ.Cake
{
    [DependsOn(
        typeof(CakeApplicationContractsModule),
        typeof(AbpAccountHttpApiClientModule),
        typeof(AbpIdentityHttpApiClientModule),
        typeof(AbpPermissionManagementHttpApiClientModule),
        typeof(AbpTenantManagementHttpApiClientModule),
        typeof(AbpFeatureManagementHttpApiClientModule)
    )]
    [DependsOn(typeof(ContentManagementHttpApiClientModule))]
    [DependsOn(typeof(SquidexManagementHttpApiClientModule))]
    public class CakeHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "Default";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(CakeApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
