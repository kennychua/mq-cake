﻿using MQ.Cake.Localization;
using Volo.Abp.Authorization.Permissions;
using Volo.Abp.Localization;

namespace MQ.Cake.Permissions
{
    public class CakePermissionDefinitionProvider : PermissionDefinitionProvider
    {
        public override void Define(IPermissionDefinitionContext context)
        {
            var myGroup = context.AddGroup(CakePermissions.GroupName);

            //Define your own permissions here. Example:
            //myGroup.AddPermission(CakePermissions.MyPermission1, L("Permission:MyPermission1"));
        }

        private static LocalizableString L(string name)
        {
            return LocalizableString.Create<CakeResource>(name);
        }
    }
}
